﻿using System;

namespace BookStore.DataLayer.Models
{
    public class Sale
    {
        public int Id { get; set; }

        public Book Book { get; set; }

        public int BookId { get; set; }

        public DateTime SoldOnUtc { get; set; }
    }
}
