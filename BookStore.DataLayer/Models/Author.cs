﻿using System.Collections.Generic;

namespace BookStore.DataLayer.Models
{
    public class Author
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public int YearOfBirth { get; set; }

        public List<Book> Books { get; set; } = new List<Book>();
    }
}