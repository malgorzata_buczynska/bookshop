﻿using System.Collections.Generic;

namespace BookStore.DataLayer.Models
{
    public class Book
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public double Price { get; set; }

        public int Year { get; set; }

        public int Stock { get; set; }

        public Author Author { get; set; }

        public int AuthorId { get; set; }

        public List<Sale> Sales { get; set; } = new List<Sale>();
    }
}
