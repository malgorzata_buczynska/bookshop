﻿using Microsoft.EntityFrameworkCore;
using BookStore.DataLayer.Models;

namespace BookStore.DataLayer
{
    public class BookStoreDBContext : DbContext
    {
        private const string _connectionString = "Data Source=.;Initial Catalog=BookStoreDB;Integrated Security=True;";

        public DbSet<Author> Authors { get; set; }
        
        public DbSet<Book> Books { get; set; }

        public DbSet<Sale> Sales { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }
    }
}
