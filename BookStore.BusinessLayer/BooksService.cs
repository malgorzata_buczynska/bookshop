﻿using BookStore.DataLayer;
using BookStore.DataLayer.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace BookStore.BusinessLayer
{
    public class BooksService
    {
        public Book AddBook(Book book)
        {
            using (var context = new BookStoreDBContext())
            {
                var entityEntry = context.Books.Add(book);
                context.SaveChanges();
                return entityEntry.Entity;
            }
        }

        public List<Book> GetAll()
        {
            using (var context = new BookStoreDBContext())
            {
                return context.Books
                    .Include(x => x.Author)
                    .Include(x => x.Sales)
                    .ToList();
            }
        }

        public List<Book> GetByAuthor(string name)
        {
            using (var context = new BookStoreDBContext())
            {
                return context.Books.Include(x => x.Author).Where(x => x.Author.Name == name || x.Author.Surname == name).ToList();
            }
        }
    }
}
