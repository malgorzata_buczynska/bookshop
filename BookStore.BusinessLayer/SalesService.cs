﻿using BookStore.DataLayer;
using BookStore.DataLayer.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BookStore.BusinessLayer
{
    public class SalesService
    {
        public Sale SellBook(int bookId)
        {
            using (var context = new BookStoreDBContext())
            {
                var book = context.Books.FirstOrDefault(x => x.Id == bookId);

                if (book == null)
                {
                    throw new Exception($"Could not find book with id: {bookId}");
                }

                if (book.Stock == 0)
                {
                    throw new Exception($"Book with id: {bookId} is out of stock!");
                }

                book.Stock--;

                var sale = new Sale();
                sale.Book = book;
                sale.SoldOnUtc = DateTime.UtcNow;

                var entityEntry = context.Sales.Add(sale);
                context.SaveChanges();
                return entityEntry.Entity;
            }
        }

        public List<Sale> GetAll()
        {
            using (var context = new BookStoreDBContext())
            {
                return context.Sales.Include(x => x.Book).Include(x => x.Book.Author).ToList();
            }
        }
    }
}
