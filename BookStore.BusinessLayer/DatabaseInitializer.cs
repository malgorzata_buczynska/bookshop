﻿using BookStore.DataLayer;

namespace BookStore.BusinessLayer
{
    public class DatabaseInitializer
    {
        public void Initialize()
        {
            using (var ctx = new BookStoreDBContext())
            {
                ctx.Database.EnsureCreated();
            }
        }
    }
}
