﻿using BookStore.DataLayer;
using BookStore.DataLayer.Models;
using System.Linq;

namespace BookStore.BusinessLayer
{
    public class AuthorsService
    {
        public Author AddAuthor(Author author)
        {
            using (var context = new BookStoreDBContext())
            {
                if (context.Authors.FirstOrDefault(x => x.Name == author.Name && x.Surname == author.Surname) != null)
                {
                    throw new System.Exception($"Author {author.Name} {author.Surname} already exists.");
                }

                var addedEntity = context.Authors.Add(author);
                context.SaveChanges();
                return addedEntity.Entity;
            }
        }

        public Author GetAuthor(string name, string surname)
        {
            using (var context = new BookStoreDBContext())
            {
                return context.Authors.FirstOrDefault(x => x.Name == name && x.Surname == surname);
            }
        }
    }
}
