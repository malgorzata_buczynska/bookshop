﻿using System;

namespace BookStore.Cli
{
    public class MenuItem
    {
        public string Description;
        public Action Action;
    }
}