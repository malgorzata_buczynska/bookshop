﻿using System;
using System.Linq;
using BookStore.BusinessLayer;
using BookStore.DataLayer.Models;

namespace BookStore.Cli
{
    public class Program
    {
        private static IoHelper _ioHelper;
        private DatabaseInitializer _databaseInitializer;
        private AuthorsService _authorsService;
        private BooksService _booksService;
        private SalesService _salesService;
        private Menu _menu;

        public Program()
        {
            _ioHelper = new IoHelper();
            _databaseInitializer = new DatabaseInitializer();
            _authorsService = new AuthorsService();
            _booksService = new BooksService();
            _salesService = new SalesService();
            _menu = new Menu();
        }

        static void Main(string[] args)
        {
            try
            {
                new Program().Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void InitializeMenuItems()
        {
            _menu.AddMenuItem(CreateAuthor,         "Add an author",                        1);
            _menu.AddMenuItem(CreateBook,           "Add a book",                           2);
            _menu.AddMenuItem(PrintAllBooks,        "Print all books",                      3);
            _menu.AddMenuItem(PrintBooksByAuthor,   "Print all books by author surname",    4);
            _menu.AddMenuItem(SellBook,             "Sell book",                            5);
            _menu.AddMenuItem(PrintSoldBooks,       "Print all sold books",                 6);
            _menu.AddMenuItem(SalesRaport,          "Print sales raport",                   7);
            _menu.AddMenuItem(Exit,                 "Exit",                                 8);
        }

        private void Run()
        {
            _databaseInitializer.Initialize();
            InitializeMenuItems();

            Console.WriteLine("Welcome to our store!");
            while (true)
            {
                _menu.ShowMenu();

                var input = _ioHelper.GetIntFromUser("Enter command number");

                Console.Clear();

                try
                {
                    _menu.ExecuteAction(input);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Command execution failed: {e.Message}");
                }
            }
        }

        private void CreateAuthor()
        {
            Author newAuthor = new Author()
            {
                Name = _ioHelper.GetStringFromUser("Enter authors name: "),
                Surname = _ioHelper.GetStringFromUser("Enter authors surname: "),
                YearOfBirth = _ioHelper.GetIntFromUser("Enter year of author's birth: ")
            };
            _authorsService.AddAuthor(newAuthor);

            Console.WriteLine($"Author added successfully");
        }

        private void CreateBook()
        {
            var authorName = _ioHelper.GetStringFromUser("Enter author name: ");
            var authorSurname = _ioHelper.GetStringFromUser("Enter author surname: ");

            Author selectedAuthor = _authorsService.GetAuthor(authorName, authorSurname);
            if (selectedAuthor == null)
            {
                Console.WriteLine($"There is no author with surname {authorSurname} and name {authorName}");
                return;
            }

            Console.WriteLine($"Author found");

            Book newBook = new Book()
            {
                AuthorId = selectedAuthor.Id,
                Title = _ioHelper.GetStringFromUser("Enter book title: "),
                Year = _ioHelper.GetIntFromUser("Enter year of publishing: "),
                Price = _ioHelper.GetDoubleFromUser("Enter the price of book: "), 
                Stock = _ioHelper.GetIntFromUser("Enter number of books in stock: ")
            };

            _booksService.AddBook(newBook);

            Console.WriteLine($"Book added successfully");
        }

        private void PrintAllBooks()
        {
            Console.WriteLine("Here is a list of all books");
            foreach (var book in _booksService.GetAll())
            {
                Console.WriteLine($"Id: {book.Id}, Author: {book.Author.Name} {book.Author.Surname} Title: {book.Title} Year: { book.Year}");
                Console.WriteLine($"Price: " + String.Format("{0:N2}", book.Price) + " On Stock: " + book.Stock);
                Console.WriteLine();
            }
        }

        private void PrintBooksByAuthor()
        {
            var authorName = _ioHelper.GetStringFromUser("Search books by author name or surname: ");

            var books = _booksService.GetByAuthor(authorName);

            Console.WriteLine($"Here is a list of books by {authorName}:");
            foreach (var book in _booksService.GetByAuthor(authorName))
            {
                Console.WriteLine();
                Console.WriteLine($" '{book.Title}', Price: " + String.Format("{0:N2}", book.Price) + ", On Stock: " + book.Stock);
                Console.WriteLine();
            }
        }

        private void SellBook()
        {
            var bookId = _ioHelper.GetIntFromUser("Enter book id you want to sell: ");

            _salesService.SellBook(bookId);

            Console.WriteLine("Book sold succesfully!");
        }

        private void PrintSoldBooks()
        {
            Console.WriteLine("Here is a list of all sold books");
            foreach (var sale in _salesService.GetAll())
            {
                Console.WriteLine();
                Console.WriteLine($"Author: {sale.Book.Author.Name} {sale.Book.Author.Surname} Title: {sale.Book.Title} Year: { sale.Book.Year}");
                Console.WriteLine($"Price: " + String.Format("{0:N2}", sale.Book.Price));
                Console.WriteLine($"Sold on (UTC): " + sale.SoldOnUtc + ", On Stock: " + sale.Book.Stock);
                Console.WriteLine();
            }
        }

        private void SalesRaport()
        {
            Console.WriteLine("Here is a sales raport");

            var allBooks = _booksService.GetAll().OrderByDescending(x => x.Sales.Count);
            var soldBooks = allBooks.Where(x => x.Sales.Any());
            var notSoldBooks = allBooks.Where(x => x.Sales.Any() == false);

            foreach (var book in soldBooks )
            {
                Console.WriteLine($"Author: {book.Author.Name} {book.Author.Surname} Title: {book.Title} Year: { book.Year}");
                Console.WriteLine($"Price: " + String.Format("{0:N2}", book.Price) + " On Stock: " + book.Stock);
                Console.WriteLine($"Number of books sold: " +book.Sales.Count + " Total income: " + String.Format("{0:N2}", book.Sales.Count * book.Price));
                Console.WriteLine();
            }

            Console.WriteLine("______________________________________");
            Console.WriteLine();

            foreach(var book in notSoldBooks)
            {
                Console.WriteLine($"Author: {book.Author.Name} {book.Author.Surname} Title: {book.Title} Year: { book.Year}");
                Console.WriteLine($"Price: " + String.Format("{0:N2}", book.Price) + " On Stock: " + book.Stock);
                Console.WriteLine($"Number of books sold: " + book.Sales.Count + " Total income: ");
                Console.WriteLine();
            }
        }

        private void Exit()
        {
            Console.WriteLine("Thank you for visiting our store! ");
            System.Threading.Thread.Sleep(3000);
            Environment.Exit(0);
        }
    }
}



