﻿using System;

namespace BookStore.Cli
{
    class IoHelper
    {
        public int GetIntFromUser(string message)
        {
            int result = 0;
            var success = false;

            while (!success)
            {
                Console.WriteLine(message);
                var input = Console.ReadLine();
                success = int.TryParse(input, out result);

                if (!success)
                {
                    Console.WriteLine("It's not a number");
                }
            }
            return result;
        }

        public bool GetBoolFromUser(string message)
        {
            bool result = false;
            var success = false;

            while (!success)
            {
                Console.WriteLine(message);
                var input = Console.ReadLine();
                success = bool.TryParse(input, out result);

                if (!success)
                {
                    Console.WriteLine("Incorrect input format");
                }
            }
            return result;
        }

        public double GetDoubleFromUser(string message)
        {
            double result = 0;
            var success = false;

            while (!success)
            {
                Console.WriteLine(message);
                var input = Console.ReadLine();
                success = double.TryParse(input, out result);

                if (!success)
                {
                    Console.WriteLine("Incorrect input format");
                }
            }
            return result;
        }

        public string GetStringFromUser(string message)
        {
            Console.WriteLine(message);
            return Console.ReadLine();
        }
    }
}
