﻿USE [BookStoreDB]
GO

INSERT INTO [dbo].[Authors]
           ([Name]
           ,[Surname]
           ,[YearOfBirth])
     VALUES
           ('Adam',' Mickiewicz',1798),
		   ('Henryk','Sienkiewicz',1846),
		   ('Boleslaw', 'Prus', 1847)
GO

USE [BookStoreDB]
GO

INSERT INTO [dbo].[Books]
           ([Title]
           ,[Price]
           ,[Year]
           ,[Stock]
           ,[AuthorId])
     VALUES
           ('Pan Tadeusz', 125, 2000, 25, 1),
		   ('Poezje', 70, 2005, 30, 1),
		   ('Konrad Wallenrod', 80, 2017, 30, 1),
		   ('Oda do młodości', 60, 2013, 20, 1),
		   ('Ogniem i mieczem', 130, 2005, 15, 2),
		   ('Pan Wołodyjowski', 85, 2015, 30, 2),
		   ('W pustyni i w puszczy', 50, 2018, 30, 2),
		   ('Krzyżacy', 60, 2005, 30, 2),
		   ('Quo vadis', 95, 2016, 25, 2),
		   ('Lalka', 100, 1997, 50, 3),
		   ('Faron', 99, 2009, 30, 3),
		   ('Michałko', 40, 2018, 20, 3)
		   
           
GO

USE [BookStoreDB]
GO

INSERT INTO [dbo].[Sales]
           ([BookId]
           ,[SoldOnUtc])
     VALUES
           (1,GETUTCDATE()),
		   (1, GETUTCDATE()),
		   (2, GETUTCDATE()),
		   (3, GETUTCDATE()),
		   (4, GETUTCDATE()),
		   (4, GETUTCDATE()),
		   (4, GETUTCDATE()),
		   (6, GETUTCDATE()),
		   (7, GETUTCDATE()),
		   (8, GETUTCDATE()),
		   (9, GETUTCDATE()),
		   (9, GETUTCDATE()),
		   (10, GETUTCDATE()),
		   (11, GETUTCDATE())

GO


